#include <iostream>
#include <vector>

using namespace std;

void Beolvas(int &n, vector<int> &u)
{
    cin >> n;
    u.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> u[i];
    }
}

int f(const int u)
{
    if(u >= 0)
    {
        return u;
    }
    else
    {
        return 0;
    }
}

void Masolas(const int n, const vector<int> u, vector<int> &v)
{
    v.resize(n);
    for(int i = 0; i < n; ++i)
    {
        v[i] = f(u[i]);
    }
}

void Kiir(const int n, const vector<int> v)
{
    for(int i = 0; i < n; i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}

int main()
{
    int n;
    vector<int> u;
    vector<int> v;

    Beolvas(n, u);

    Masolas(n, u, v);

    Kiir(n, v);

    return 0;
}
