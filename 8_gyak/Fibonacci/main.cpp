#include <iostream>

using namespace std;

//Baratsagos es biztonsagos beolvasas
void Beolvas(int &n)
{
    bool jo;
    do
    {
        cout << "Add meg n erteket (n>0): " << endl;
        cin >> n;
        jo = n > 0;
        if(!jo)
        {
            cout << "Nem jo (n>0)." << endl;
        }
    }while(!jo);
}

//Rekurziv megoldas: nagy n eseten lassu
int FibRek(const int n)
{
    if(n == 1 || n == 2)
    {
        return 1;
    }
    else
    {
        return FibRek(n-1) + FibRek(n-2);
    }
}

//Iterativ megoldas
double Fib(const int n)
{
    double elozo = 1;
    double aktualis = 1;
    for(int i = 3; i <= n; i++)
    {
        double uj = elozo + aktualis;
        elozo = aktualis;
        aktualis = uj;
    }
    return aktualis;
}

void Kiir(const int n, const double f)
{
    cout << "A(z) " << n << ". Fibonacci szam: " << f << ".";
}

int main()
{
    int n;
    double f;

    Beolvas(n);

    //f = FibRek(n);
    f = Fib(n);

    Kiir(n, f);

    return 0;
}
