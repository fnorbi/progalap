#include <iostream>
#include <cmath>

using namespace std;

const int MAXN = 1000;

struct Vektor
{
    double x, y;
};

void Beolvas(int &n, Vektor a[])
{
    cin >> n;
    for(int i = 0; i < n; i++)
    {
        cin >> a[i].x >> a[i].y;
    }
}

bool hosszu(const Vektor v)
{
    return sqrt(v.x * v.x + v.y * v.y) > 5;
}

void Norma(const int n, const Vektor a[], int &db, int hosszuak[])
{
    db = 0;
    for(int i = 0; i < n; i++)
    {
        if(hosszu(a[i]))
        {
            hosszuak[db] = i;
            db++;
        }
    }
}

void Kiir(const int db, const int hosszuak[])
{
    cout << db << endl;
    for(int i = 0; i < db; i++)
    {
        cout << hosszuak[i] + 1 << " ";
    }
}

int main()
{
    int n;
    Vektor a[MAXN];
    int db;
    int hosszuak[MAXN];

    Beolvas(n, a);

    Norma(n, a, db, hosszuak);

    Kiir(db, hosszuak);

    return 0;
}
