#include <iostream>
#include <vector>

using namespace std;

void beolvas(int &N, vector<int> &v)
{
    cin >> N;
    for(int i = 0; i < N; i++)
    {
        int szam;
        cin >> szam;
        v.push_back(szam);
    }
}

void balraTol(int n, vector<int> t, vector<int> &eltolt)
{
    eltolt.resize(n);
    eltolt[n-1] = t[0];
    for(int i = 0; i < n-1; i++)
    {
        eltolt[i] = t[i+1];
    }
}

void balra(int n, vector<int> &t, vector<int> &eltolt)
{
    eltolt.resize(n);
    eltolt[n-1] = t[0];
    for(int i = 0; i < n-1; i++)
    {
        eltolt[i] = t[i+1];
    }
    t = eltolt;
}

void kiir(int n, vector<int> eltolt)
{
    for(int i = 0; i < n; i++)
    {
        cout << eltolt[i] << " ";
    }
}

int main()
{
    int n;
    vector<int> t;
    vector<int> eltolt;

    beolvas(n, t);
    /*
    balraTol(n, t, eltolt);
    balraTol(n, eltolt, t);
    balraTol(n, t, eltolt);
    */

    for(int i = 0; i < 10; i++)
    {
        balra(n, t, eltolt);
        kiir(n, eltolt);
        cout << endl;
    }

    return 0;
}
