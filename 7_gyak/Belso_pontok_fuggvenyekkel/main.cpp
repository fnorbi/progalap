#include <iostream>
#include <vector>

using namespace std;

struct Pont
{
    double x, y;
};

double tavNegyzet(Pont p, Pont c)
{
    return (p.x-c.x)*(p.x-c.x)+(p.y-c.y)*(p.y-c.y);
}

bool belsoPontE(Pont p, double u, double v, double r)
{
    Pont c;
    c.x = u;
    c.y = v;
    return tavNegyzet(p, c) < r*r;
}

void beolvas(int &n, double &r, double &u, double &v, vector<Pont> &pontok)
{
    cin >> n >> r >> u >> v;
    pontok.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> pontok[i].x >> pontok[i].y;
    }
}

void kivalogat(int n, vector<Pont> pontok, double u, double v, double r, int &db, vector<int> &belsok)
{
    db = 0;
    for(int i = 0; i < n; i++)
    {
        Pont p = pontok[i];
        if(belsoPontE(p, u, v, r))
        {
            db++;
            belsok.push_back(i);
        }
    }
}

void kivalogat2(int n, vector<Pont> pontok, double u, double v, double r, int &db, int belsokTomb[1000])
{
    db = 0;
    for(int i = 0; i < n; i++)
    {
        Pont p = pontok[i];
        if(belsoPontE(p, u, v, r))
        {
            belsokTomb[db] = i;
            db++;
        }
    }
}

void kiiras(int db, vector<int> belsok)
{
    cout << "A pontok szama: " << endl;
    cout << db << endl;
    cout << "A belso pontok sorszamai: " << endl;
    for(int i = 0; i < db; i++)
    {
        cout << belsok[i]+1 << " ";
    }
    cout << endl;
}

void kiiras2(int db, int belsokTomb[1000])
{
    cout << "A pontok szama: " << endl;
    cout << db << endl;
    cout << "A belso pontok sorszamai: " << endl;
    for(int i = 0; i < db; i++)
    {
        cout << belsokTomb[i]+1 << " ";
    }
    cout << endl;
}

int main()
{
    //Deklaracio
    int n;
    vector<Pont> pontok;
    double u, v, r;

    beolvas(n, r, u, v, pontok);
    cout << n << endl;

    int db;
    vector<int> belsok;
    int belsokTomb[1000];

    kivalogat(n, pontok, u, v, r, db, belsok);
    kivalogat2(n, pontok, u, v, r, db, belsokTomb);

    kiiras(db, belsok);
    kiiras2(db, belsokTomb);

    return 0;
}
