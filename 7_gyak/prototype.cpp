#include <iostream>

using namespace std;

int f(int a);
int g(int a);

int g(int a)
{
    a = 5;
    return f(a);
}

int f(int a)
{
    if(a == 5)
    {
        return 5;
    }
    else
    {
        return g(a);
    }
}

int main()
{
    cout << f(2);
    return 0;
}