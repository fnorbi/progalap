#include <iostream>
#include <vector>
#include <ctime>

using namespace std;

/*
const int N = 5;
const int M = 7;
*/

void beolvas(int &N, int &M, vector<vector<int>> &m)
{
    cin >> N >> M;
    m.resize(N);
    for(int i = 0; i < N; i++)
    {
        m[i].resize(M);
    }
}

void feltolt(int N, int M, vector<vector<int>> &m)
{
    srand(time(0));
    for(int i = 0; i < N; i++)
    {
        for(int j = 0; j < M; j++)
        {
            m[i][j] = rand() % 10 + 1;
        }
    }
}

void kiir(int N, int M, vector<vector<int>> m)
{
    for(int i = 0; i < N; i++)
    {
        for(int j = 0; j < M; j++)
        {
            cout << m[i][j] << " ";
        }
        cout << endl;
    }
}

int maximum(int i, int M, vector<vector<int>> m)
{
    int maxe = m[i][0];
    for(int j = 1; j < M; j++)
    {
        if(m[i][j] > maxe)
        {
            maxe = m[i][j];
        }
    }
    return maxe;
}

void soronkentMaximum(int N, int M, vector<vector<int>> m, vector<int> &maximumok)
{
    maximumok.resize(N);
    for(int i = 0; i < N; i++)
    {
        maximumok[i] = maximum(i, M, m);
    }
}

void maxKiir(int n, vector<int> t)
{
    for(int i = 0; i < n; i++)
    {
        cout << t[i] << endl;
    }
}

int main()
{
    /*
    int m[N][M];

    m[0][0] = 23; //m[i][j] = (i+1). sor (j+1). eleme

    //cout << m[0][0];

    for(int i = 0; i < N; i++)
    {
        for(int j = 0; j < M; j++)
        {
            m[i][j] = i + j;
        }
    }

    for(int i = 0; i < N; i++)
    {
        for(int j = 0; j < M; j++)
        {
            cout << m[i][j] << " ";
        }
        cout << endl;
    }
    */

    int N, M;
    vector<vector<int>> m;
    vector<int> maximumok;

    beolvas(N, M, m);
    feltolt(N, M, m);
    kiir(N, M, m);
    soronkentMaximum(N, M, m, maximumok);
    maxKiir(N, maximumok);

    return 0;
}
