#include <iostream>
#include <vector>

using namespace std;

struct Adat
{
    string mi;
    string mit;
};

void beolvas(int &n, vector<Adat> &adatok)
{
    cin >> n;
    adatok.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> adatok[i].mi >> adatok[i].mit;
    }
}

bool eleme(string e, vector<string> v)
{
    int i = 0;
    while(i < v.size() && e != v[i])
    {
        i++;
    }
    return i < v.size();
}

void allatokKivalogatasa(vector<Adat> adatok, vector<string> &allatok)
{
    for(int i = 0; i < adatok.size(); i++)
    {
        if(!eleme(adatok[i].mi, allatok))
        {
            allatok.push_back(adatok[i].mi);
        }
    }
}

void kiir(vector<string> v)
{
    cout << v.size() << endl;
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << endl;
    }
}

bool eszikEAllatot(vector<Adat> adatok, string allat, vector<string> allatok)
{
    int i = 0;
    while(i < adatok.size() && !(adatok[i].mi == allat && eleme(adatok[i].mit, allatok)))
    {
        i++;
    }
    return i < adatok.size();
}

void allatEvokKivalogatasa(vector<Adat> adatok, vector<string> allatok,
                           vector<string> &eszikAllatot)
{
    for(int i = 0; i < allatok.size(); i++)
    {
        if(eszikEAllatot(adatok, allatok[i], allatok) && !eleme(allatok[i], eszikAllatot))
        {
            eszikAllatot.push_back(allatok[i]);
        }
    }
}

void allatotNemEvokKivalogatasa(vector<string> allatok, vector<string> eszikAllatot,
                                vector<string> &nemEszikAllatot)
{
    for(int i = 0; i < allatok.size(); i++)
    {
        if(!eleme(allatok[i], eszikAllatot))
        {
            nemEszikAllatot.push_back(allatok[i]);
        }
    }
}

int main()
{
    /*
    string s;
    cin >> s;
    cout << stoi(s)+1;
    */

    int n;
    vector<Adat> adatok;
    vector<string> allatok;
    vector<string> eszikAllatot;
    vector<string> nemEszikAllatot;

    beolvas(n, adatok);
    allatokKivalogatasa(adatok, allatok);
    allatEvokKivalogatasa(adatok, allatok, eszikAllatot);
    allatotNemEvokKivalogatasa(allatok, eszikAllatot, nemEszikAllatot);
    //kiir(allatok);
    //kiir(eszikAllatot);
    kiir(nemEszikAllatot);

    return 0;
}
