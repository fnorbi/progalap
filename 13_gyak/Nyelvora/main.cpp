#include <iostream>
#include <vector>

using namespace std;

struct Nyelvtanulas
{
    int tanulo;
    int nyelv;
    int eredmeny;
};

void beolvas(int &n, int &m, int &k, vector<Nyelvtanulas> &nyelvtanulasok)
{
    cin >> n >> m >> k;
    nyelvtanulasok.resize(n);
    for (int i = 0; i < n; i++)
    {
        cin >> nyelvtanulasok[i].tanulo;
        cin >> nyelvtanulasok[i].nyelv;
        cin >> nyelvtanulasok[i].eredmeny;
    }
}

// Maximum kivalasztas eredmeny mezo szerint
void feladat1(vector<Nyelvtanulas> nyelvtanulasok)
{
    int maxi = 0;
    for (int i = 2; i < nyelvtanulasok.size(); i++)
    {
        if(nyelvtanulasok[i].eredmeny > nyelvtanulasok[maxi].eredmeny)
        {
            maxi = i;
        }
    }
    cout << nyelvtanulasok[maxi].tanulo << " " << nyelvtanulasok[maxi].nyelv << endl;
}

// Letrehozzuk a szamlalo tombot, beallitjuk kezdetben minden elemet 0-ra,
// majd megnoveljuk minden nyelv eseten a megfelelo elemet sorszamu elemet egyel
void szamlalas(vector<Nyelvtanulas> nyelvtanulasok, int k, vector<int> &hanyanTanuljak)
{
    hanyanTanuljak.resize(k);
    for (int i = 0; i < k; i++)
    {
        hanyanTanuljak[i] = 0;
    }
    
    for (int i = 0; i < nyelvtanulasok.size(); i++)
    {
        int nyelv = nyelvtanulasok[i].nyelv;
        hanyanTanuljak[nyelv-1]++;
    }
}

// Most mar a feladat: maximum kivalasztas a seged tombre
void feladat2(vector<int> hanyanTanuljak)
{
    int maxi = 0;
    for (int i = 1; i < hanyanTanuljak.size(); i++)
    {
        if(hanyanTanuljak[i] > hanyanTanuljak[maxi])
        {
            maxi = i;
        }
    }
    cout << maxi + 1 << endl;
}

// Kivalogatjuk amit senki nem valasztott, azaz 0 ember tanulja
void feladat3(vector<int> hanyanTanuljak)
{
    vector<int> nyelvek;
    for (int i = 0; i < hanyanTanuljak.size(); i++)
    {
        if(hanyanTanuljak[i] == 0)
        {
            nyelvek.push_back(i+1);
        }
    }
    
    cout << nyelvek.size() << " ";
    for (int i = 0; i < nyelvek.size(); i++)
    {
        cout << nyelvek[i] << " ";
    }
    cout << endl;
}

// 2 rekord csereje mezonkent ertendo
void csere(Nyelvtanulas &a, Nyelvtanulas &b)
{
    Nyelvtanulas seged = a;
    a = b;
    b = seged;
}

// Rendezzuk a vektort a tanulok sorszamai szerint novekvoen (pl.: minimum kivalasztasos rendezes)
void rendezes(vector<Nyelvtanulas> &nyelvtanulasok)
{
    for (int i = 0; i < nyelvtanulasok.size()-1; i++)
    {
        int mini = i;
        for (int j = i+1; j < nyelvtanulasok.size(); j++)
        {
            if(nyelvtanulasok[j].tanulo < nyelvtanulasok[mini].tanulo)
            {
                mini = j;
            }
        }
        csere(nyelvtanulasok[mini], nyelvtanulasok[i]);
    }
}

// Eldontes tetel: mind adott tulajdonsagu-e?
bool jo(int tanulo, vector<Nyelvtanulas> nyelvtanulasok)
{
    int j = 0;
    while(j < nyelvtanulasok.size() && (nyelvtanulasok[j].eredmeny >= 90 || nyelvtanulasok[j].tanulo != tanulo))
    {
        j++;
    }
    return j >= nyelvtanulasok.size();
}

// Keresunk olyan tanulot akinek minden eredmenye >= 90
void feladat4(vector<Nyelvtanulas> nyelvtanulasok)
{
    int i = 0;
    while(i < nyelvtanulasok.size() && !jo(nyelvtanulasok[i].tanulo, nyelvtanulasok))
    {
        i++;
    }

    // a rendezes miatt az elsonek megtalalt lesz a legkisebb sorszamu tanulo
    if(i < nyelvtanulasok.size())
    {
        cout << nyelvtanulasok[i].tanulo << endl;
    }
    else
    {
        cout << -1 << endl;
    }
}

bool volt(int tanulo, vector<int> tanulok)
{
    int j = 0;
    while(j < tanulok.size() && tanulo != tanulok[j])
    {
        j++;
    }
    return j < tanulok.size();
}

// Halmazkeszites: csak adott tulajdonsagu elemeket valogassunk ki, mindet csak egyszer
// a tanulok sorszamainak novekedesere a rendezes miatt nem kell figyelnunk, automatikusan teljesul
void feladat5(vector<Nyelvtanulas> nyelvtanulasok)
{
    vector<int> tanulok;
    for (int i = 0; i < nyelvtanulasok.size(); i++)
    {
        if(nyelvtanulasok[i].eredmeny == 100 && !volt(nyelvtanulasok[i].tanulo, tanulok))
        {
            tanulok.push_back(nyelvtanulasok[i].tanulo);
        }
    }
    
    cout << tanulok.size() << " ";
    for (int i = 0; i < tanulok.size(); i++)
    {
        cout << tanulok[i] << " ";
    }
    cout << endl;
}

int main()
{
    // n: rekordok szama
    // m: tanulok szama
    // k: nyelvek szama 
    int n, m, k;
    vector<Nyelvtanulas> nyelvtanulasok;

    beolvas(n, m, k, nyelvtanulasok);

    feladat1(nyelvtanulasok);

    //Keves nyelv van, szamoljuk meg melyiket hanyan tanuljak (szamlalo tomb)
    //hanyanTanuljak[i]: az i+1. sorszamu nyelvet hanyan tanuljak
    vector<int> hanyanTanuljak;
    szamlalas(nyelvtanulasok, k, hanyanTanuljak);

    feladat2(hanyanTanuljak);

    feladat3(hanyanTanuljak);

    //A 4. es 5. feladathoz erdemes volna rendezni a bemenetet a tanulok sorszamai szerint novekvoen
    rendezes(nyelvtanulasok);

    feladat4(nyelvtanulasok);

    feladat5(nyelvtanulasok);

    return 0;
}
