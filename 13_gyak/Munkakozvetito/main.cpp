#include <iostream>
#include <vector>

using namespace std;

struct Allas
{
    int fiz, ho;
    string nev;
};

void beolvas(int &n, int &kp, vector<Allas> &allasok)
{
    cin >> n >> kp;
    allasok.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> allasok[i].fiz >> allasok[i].ho;
        cin.ignore();
        getline(cin, allasok[i].nev);
    }

    /*
    for(int i = 0; i < allasok.size(); i++)
    {
        //cout << allasok[i].fiz << " " << allasok[i].ho << " ";
        cout << allasok[i].nev << endl;
    }
    */
}

void feladat1(vector<Allas> allasok)
{
    cout << "#" << endl;
    int maxi = 0;
    for(int i = 1; i < allasok.size(); i++)
    {
        if(allasok[i].fiz > allasok[maxi].fiz)
        {
            maxi = i;
        }
    }
    cout << allasok[maxi].nev << endl;
}

struct Munkas
{
    string nev;
    int osszfiz;
};

int hanyadik(string nev, vector<Munkas> munkasok)
{
    int i = 0;
    while(i < munkasok.size() && nev != munkasok[i].nev)
    {
        i++;
    }
    return i;
}

void feladat2(vector<Allas> allasok)
{
    cout << "#" << endl;
    vector<Munkas> munkasok;
    for(int i = 0; i < allasok.size(); i++)
    {
        int sorszam = hanyadik(allasok[i].nev, munkasok);
        if(sorszam >= munkasok.size())
        {
            ///Uj munkast kell felvenni
            Munkas munkas;
            munkas.nev = allasok[i].nev;
            munkas.osszfiz = 100000 * allasok[i].fiz * allasok[i].ho;
            munkasok.push_back(munkas);
        }
        else
        {
            ///Meglevo munkas osszfizeteset kell novelni
            munkasok[sorszam].osszfiz += 100000 * allasok[i].fiz * allasok[i].ho;
        }
    }

    cout << munkasok.size() << endl;
    for(int i = 0; i < munkasok.size(); i++)
    {
        cout << munkasok[i].nev << " " << munkasok[i].osszfiz << endl;
    }
}

bool jo(string nev, vector<Allas> allasok)
{
    int maxfiz = -1;
    bool eddigjo = true;
    int db = 0;
    int j = 0;
    while(j < allasok.size() && eddigjo)
    {
        if(nev == allasok[j].nev)
        {
            db++;
            if(allasok[j].fiz > maxfiz)
            {
                maxfiz = allasok[j].fiz;
            }
            else
            {
                eddigjo = false;
            }
        }
        j++;
    }
    return eddigjo && db >= 2;
}

void feladat3(vector<Allas> allasok)
{
    cout << "#" << endl;
    int i = 0;
    while(i < allasok.size() && !jo(allasok[i].nev, allasok))
    {
        i++;
    }

    if(i < allasok.size())
    {
        cout << allasok[i].nev << endl;
    }
    else
    {
        cout << "NINCS" << endl;
    }
}

bool volt(string nev, vector<string> nevek)
{
    int i = 0;
    while(i < nevek.size() && nev != nevek[i])
    {
        i++;
    }
    return i < nevek.size();
}

void feladat4(vector<Allas> allasok, int kp)
{
    cout << "#" << endl;
    vector<string> magasfiz;
    for(int i = 0; i < allasok.size(); i++)
    {
        if(allasok[i].fiz > kp && !volt(allasok[i].nev, magasfiz))
        {
            magasfiz.push_back(allasok[i].nev);
        }
    }
    cout << magasfiz.size() << endl;
    for (int i = 0; i < magasfiz.size(); i++)
    {
        cout << magasfiz[i] << endl;
    }
}

int main()
{
    int n, kp;
    vector<Allas> allasok;

    beolvas(n, kp, allasok);

    feladat1(allasok);

    feladat2(allasok);

    feladat3(allasok);

    feladat4(allasok, kp);

    return 0;
}
