#include <iostream>

using namespace std;

const int MINN = 0;
const int MAXN = 100;

//#define BIRO;

int beEgesz(const string uzenet, const int MIN, const int MAX, const string hibaUzenet)
{
    int n;
    bool hiba;
    do
    {
        cout << uzenet << " [" << MIN << ", " << MAX << "]: ";
        cin >> n;
        string temp = "";
        if(cin.fail())
        {
            cin.clear();
            getline(cin, temp);
        }
        hiba = temp != "" || cin.fail() || n < MIN || n > MAX;
        if(hiba)
        {
            cout << hibaUzenet << " [" << MIN << ", " << MAX << "]" << endl;
        }
    }while(hiba);

    return n;
}

void beolvas(int &n)
{
    #ifdef BIRO

    cin >> n;

    #else

    string uzenet = "Adja meg n erteket";
    string hibaUzenet = "Nem jo!";
    n = beEgesz(uzenet, MINN, MAXN, hibaUzenet);

    #endif
}

int main()
{
    int n;

    beolvas(n);

    cout << n << endl;

    return 0;
}
