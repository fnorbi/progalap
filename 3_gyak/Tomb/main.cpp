#include <iostream>

using namespace std;

const int MAXN = 5;

int main()
{

    //5 elemű tömbök deklarációja
    int a[5] = {1, 2, 3, 4, 5};
    int b[5] = {6, 7, 8, 9, 10};

    //"a" mutat az első elem memória címére, *(a+1) az első elemtől után következő (tehát 2. elem) értéke
    cout << *(a+1);
    cout << a[5];

    cout << a << " " << b << " " << b-a << endl;
    cout << *(a+(b-a)) << " " << a[b-a] << " " << a[-8] << endl;

    cout << b[0] << endl;
    a[-8] = 42;
    cout << b[0] << endl;

    b[8] = 73;
    cout << a[0] << endl;


    //Olvassunk be 5 szamot es irassuk ki oket!
    int tomb[MAXN];
    for(int i = 0; i < MAXN; i++)
    {
        /*
        int a;
        cin >> a;
        tomb[i] = a;
        */
        cin >> tomb[i];
    }

    for(int i = 0; i < MAXN; i++)
    {
        cout << tomb[i] << " ";
    }

    return 0;
}
