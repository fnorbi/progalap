#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n;
    vector<int> tomeg;

    int osszeg;
    int szOsszeg, nDarab;
    double atlag;

    osszeg = 0; szOsszeg = 0; nDarab = 0;

    //Beolvasas
    bool jo;
    do
    {
        ///cout << "N: ";
        cin >> n;
        jo = n > 0;
        if(!jo)
        {
            cout << "Nem jo: n > 0" << endl;
        }
    }while(!jo);

    for(int i = 0; i < n; i++)
    {
        int seged;
        do
        {
            ///cout << "Add meg az " << i+1 << ". tomeget: ";
            cin >> seged;
            jo = 0 < seged && seged <= 500;
            if(!jo)
            {
                cout << "Nem jo: 0 < tomeg <= 500" << endl;
            }
        }while(!jo);
        tomeg.push_back(seged);
    }

    //Feldolgozas = osszegzes tetel
    for(int i = 0; i < n; i++)
    {
        //osszeg = osszeg + tomeg[i];
        osszeg += tomeg[i];
    }

    //felteteles osszegzes + megszamolas
    for(int i = 0; i < n; i++)
    {
        if(tomeg[i] <= 100)
        {
            //szOsszeg = szOsszeg + tomeg[i];
            szOsszeg += tomeg[i];
            //nDarab = nDarab + 1;
            //nDarab += 1;
            nDarab++;
        }
    }

    atlag = (double)szOsszeg/nDarab;

    //Kiiratas
    ///cout << "Osszesen " << osszeg << "kg papirt hoztak!" << endl;
    cout << osszeg << endl;

    ///cout << "Legfeljebb 100kg-ok atlaga: " << atlag << "kg" << endl;
    cout << atlag << endl;

    return 0;
}
