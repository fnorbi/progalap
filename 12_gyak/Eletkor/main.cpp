#include <iostream>
#include <vector>

using namespace std;

const int KOR = 150;

void beolvas(int &n, vector<int> &eletkorok)
{
    cin >> n;
    eletkorok.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> eletkorok[i];
    }
}

void szamlalas(vector<int> eletkorok, vector<int> &darabszamok)
{
    darabszamok.resize(KOR);

    for(int i = 0; i < KOR; i++)
    {
        darabszamok[i] = 0;
    }

    for(int i = 0; i < eletkorok.size(); i++)
    {
        int kor = eletkorok[i];
        darabszamok[kor - 1]++;
    }

    /*
    for(int i = 0; i < KOR; i++)
    {
        cout << darabszamok[i] << " ";
    }
    cout << endl;
    */
}

void kozepso(int n, vector<int> darabszamok)
{
    int kor = 0;
    int s = 0;
    while(s <= n/2)
    {
        s += darabszamok[kor];
        kor++;
    }

    cout << kor << endl;
}

int main()
{
    int n;
    vector<int> eletkorok;

    beolvas(n, eletkorok);

    vector<int> darabszamok; //darabszamok[i]: i+1. korban hany ember van
    //szamlalo tomb technika
    szamlalas(eletkorok, darabszamok);

    kozepso(n, darabszamok);

    return 0;
}
