#include <iostream>
#include <vector>

using namespace std;

void beolvas(int &n, vector<int> &pontszamok)
{
    cin >> n;
    pontszamok.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> pontszamok[i];
    }
}

void csere(int &a, int &b)
{
    int seged = a;
    a = b;
    b = seged;
}

void rendezes(vector<int> &pontszamok)
{
    for(int i = 0; i < pontszamok.size() - 1; i++)
    {
        int mini = i;
        for(int j = i+1; j < pontszamok.size(); j++)
        {
            if(pontszamok[j] < pontszamok[mini])
            {
                mini = j;
            }
        }
        csere(pontszamok[i], pontszamok[mini]);
    }
}

int main()
{
    int n;
    vector<int> pontszamok;

    beolvas(n, pontszamok);

    rendezes(pontszamok);

    cout << pontszamok[n/2];

    return 0;
}
