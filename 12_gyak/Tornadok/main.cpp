#include <iostream>
#include <vector>

using namespace std;

void beolvas(int &n, int &t, vector<int> &tornadok)
{
    cin >> n >> t;
    tornadok.resize(t);
    for(int i = 0; i < t; i++)
    {
        cin >> tornadok[i];
    }
}

void szamlalas(vector<int> &napok, vector<int> tornadok)
{
    for(int i = 0; i < napok.size(); i++)
    {
        napok[i] = 0;
    }

    for(int i = 0; i < tornadok.size(); i++)
    {
        int nap = tornadok[i];
        napok[nap-1]++;
    }

    /*
    for(int i = 0; i < napok.size(); i++)
    {
        cout << napok[i] << " ";
    }
    */
}

void feladat1(vector<int> napok)
{
    cout << "#" << endl;
    int db = 0;
    for(int i = 0; i < napok.size(); i++)
    {
        if(napok[i] == 0)
        {
            db++;
        }
    }
    cout << db << endl;
}

void feladat2(vector<int> napok)
{
    cout << "#" << endl;
    int i = 1;
    while(i < napok.size() - 1 && !(napok[i-1] == 1 && napok[i] == 1 && napok[i+1] == 1))
    {
        i++;
    }

    if(i < napok.size() - 1)
    {
        cout << i+1 << endl;
    }
    else
    {
        cout << 0 << endl;
    }
}

void feladat3(int n, vector<int> tornadok)
{
    cout << "#" << endl;
    int maxe = tornadok[0]-1;
    for(int i = 1; i < tornadok.size(); i++)
    {
        if(tornadok[i] - tornadok[i-1] - 1 > maxe)
        {
            maxe = tornadok[i] - tornadok[i-1] - 1;
        }
    }

    int utsoTornado = tornadok.size();
    if(n - tornadok[utsoTornado-1] > maxe)
    {
        maxe = n - tornadok[utsoTornado-1];
    }
    cout << maxe << endl;
}

int main()
{
    int n, t;
    vector<int> tornadok;

    beolvas(n, t, tornadok);

    //szamlalo tomb technika
    vector<int> napok; //napok[i]: i+1. napon hany tornado volt
    napok.resize(n);
    szamlalas(napok, tornadok);

    feladat1(napok);

    feladat2(napok);

    feladat3(n, tornadok);

    cout << "#\n\n#\n\n";

    return 0;
}
