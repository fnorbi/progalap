#include <iostream>
#include <vector>

using namespace std;

struct Bor
{
    int menny;
    int ar;

    int bevetel()
    {
        return menny*ar;
    }
};

void beolvas(int &n, vector<Bor> &borok)
{
    cin >> n;
    borok.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> borok[i].menny >> borok[i].ar;
    }
}

void feladat1(vector<Bor> borok)
{
    int mini = 0;
    for(int i = 1; i < borok.size(); i++)
    {
        if(borok[i].menny < borok[mini].menny)
        {
            mini = i;
        }
    }
    cout << mini + 1 << endl;
}

void feladat2(vector<Bor> borok)
{
    int maxe = -1;
    for(int i = 0; i < borok.size(); i++)
    {
        if(borok[i].menny > 1000)
        {
            if(borok[i].ar > maxe)
            {
                maxe = borok[i].ar;
            }
        }
    }
    cout << maxe << endl;
}

bool eleme(int ar, vector<int> arak)
{
    int i = 0;
    while(i < arak.size() && ar != arak[i])
    {
        i++;
    }
    return i < arak.size();
}

void feladat3(vector<Bor> borok)
{
    vector<int> arak;
    for(int i = 0; i < borok.size(); i++)
    {
        if(!eleme(borok[i].ar, arak))
        {
            arak.push_back(borok[i].ar);
        }
    }
    cout << arak.size() << endl;
}

bool jo(int i, vector<Bor> borok)
{
    int j = 0;
    while(j < i && !(borok[i].menny <= borok[j].menny))
    {
        j++;
    }
    return j >= i;
}

void feladat4(vector<Bor> borok)
{
    vector<int> evek;
    /*
    int maxmenny = borok[0].menny;
    for(int i = 1; i < borok.size(); i++)
    {
        if(borok[i].menny > maxmenny)
        {
            maxmenny = borok[i].menny;
            evek.push_back(i);
        }
    }
    */

    for(int i = 1; i < borok.size(); i++)
    {
        if(jo(i, borok))
        {
            evek.push_back(i);
        }
    }

    cout << evek.size() << " ";
    for(int i = 0; i < evek.size(); i++)
    {
        cout << evek[i] + 1 << " ";
    }
    cout << endl;
}

int main()
{
    int n;
    vector<Bor> borok;

    beolvas(n, borok);

    feladat1(borok);

    feladat2(borok);

    feladat3(borok);

    feladat4(borok);

    return 0;
}
