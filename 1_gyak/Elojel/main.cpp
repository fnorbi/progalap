#include <iostream>

using namespace std;

int main()
{

    double x;
    int sgn;

    cout << "Adj meg egy valos szamot: ";
    cin >> x;

    /*
    if(x < 0)
    {
        sgn = -1;
    }
    else
    {
        if(x == 0)
        {
            sgn = 0;
        }
        else
        {
            sgn = 1;
        }
    }
    */

    if(x < 0)
    {
        sgn = -1;
    }
    else if(x == 0)
    {
        sgn = 0;
    }
    else
    {
        sgn = 1;
    }

    cout << "A megadott szam (" << x << ") elojele: " << sgn << endl;

    //cout << ((0.1 + 0.2) == 0.3) << endl;

    return 0;
}
