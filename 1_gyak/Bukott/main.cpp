#include <iostream>

using namespace std;

int main()
{
    //Deklar�ci�
    int jegy;
    bool bukottE;

    //Beolvas�s
    cout << "Add meg a jegyet: ";
    cin >> jegy;
    if(jegy<1 || jegy>20)
    {
        cout << "A jegynek 1 es 20 kozott kell lennie!";
        exit(1);
    }

    //Feldolgoz�s
    //bukottE = jegy <= 10;
    if(jegy <= 10)
    {
        bukottE = true;
    }
    else
    {
        bukottE = false;
    }

    //Ki�r�s
    //cout << bukottE << endl;
    if(bukottE)
    {
        cout << "Megbukott!" << endl;
    }
    else
    {
        cout << "Nem bukott meg!" << endl;
    }

    return 0;
}
