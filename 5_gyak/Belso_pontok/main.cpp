#include <iostream>
#include <vector>

using namespace std;

struct Pont
{
    double x, y;
};

///const int MAXN = 1000;

int main()
{
    //Deklaracio
    int n;
    vector<Pont> pontok; ///int pontok[MAXN];
    double u, v, r;

    //Beolvasas
    cin >> n >> r >> u >> v;
    pontok.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> pontok[i].x >> pontok[i].y;
        //Ha nem akarom atmeretezni a vektort
        /*
        Pont p;
        cin >> p.x >> p.y;
        pontok.push_back(p);
        */
    }

    int db = 0;
    vector<int> belsok;
    ///int belsokT[MAXN];
    //Feldolgozas
    for(int i = 0; i < n; i++)
    {
        Pont p = pontok[i];
        if((p.x-u)*(p.x-u)+(p.y-v)*(p.y-v) < r*r)
        {
            ///belsokT[db] = i;
            db++;
            belsok.push_back(i);
        }
    }

    //Kiiras
    cout << "A pontok szama: " << endl;
    cout << db << endl;
    cout << "A belso pontok sorszamai: " << endl;
    for(int i = 0; i < db; i++)
    {
        cout << belsok[i]+1 << " ";
    }
    cout << endl;
    cout << "A belso pontok x koordinatai: " << endl;
    for(int i = 0; i < db; i++)
    {
        cout << pontok[belsok[i]].x << " ";
    }
    cout << endl;

    return 0;
}
