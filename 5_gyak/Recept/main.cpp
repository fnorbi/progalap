#include <iostream>
#include <vector>

using namespace std;

struct Anyag
{
    int sorsz, menny;
};

int main()
{
    int n;
    vector<int> arak;
    int db;
    vector<Anyag> anyagok;

    cin >> n;
    arak.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> arak[i];
    }
    cin >> db;
    anyagok.resize(db);
    for(int i = 0; i < db; i++)
    {
        cin >> anyagok[i].sorsz >> anyagok[i].menny;
    }

    int koltseg = 0;
    for(int i = 0; i < db; i++)
    {
        int sorsz = anyagok[i].sorsz - 1;
        koltseg += anyagok[i].menny * arak[sorsz];
    }

    cout << koltseg << endl;

    return 0;
}
