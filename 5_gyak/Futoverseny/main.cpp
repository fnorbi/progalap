#include <iostream>
#include <vector>

using namespace std;

struct Meres
{
    int ind, erk;
};

int main()
{
    int n;
    vector<Meres> meresek;

    //Beolvasas
    cin >> n;
    meresek.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> meresek[i].ind >> meresek[i].erk;
    }

    //Feldolgozas => minimum-kivalasztas
    int minindex = 0;
    int minido = meresek[0].erk - meresek[0].ind;
    for(int i = 1; i < n; i++)
    {
        if(meresek[i].erk - meresek[i].ind < minido)
        {
            minindex = i;
            minido = meresek[i].erk - meresek[i].ind;
        }
    }

    //Eldontes
    bool holtverseny;
    int i = 0;
    while(i < n && !(i != minindex && meresek[i].erk-meresek[i].ind == minido))
    {
        i++;
    }
    holtverseny = i < n;

    //Kiiratas
    cout << minindex+1 << " " << minido << endl;
    if(holtverseny)
    {
        cout << "Volt holtverseny!" << endl;
    }
    else
    {
        cout << "Nem volt holtverseny!" << endl;
    }

    return 0;
}
