#include <iostream>
#include <ctime>

using namespace std;

const int N = 10000;

int main()
{
    srand(time(0));
    int dobas;

    int gyakorisagok[6] = {0, 0, 0, 0, 0, 0};

    for(int i = 1; i <= N; i++)
    {
        //Random szam 1 es 6 kozott
        dobas = rand() % 6 + 1;
        gyakorisagok[dobas-1]++;
        cout << dobas << " ";
        if(i % 20 == 0)
        {
            cout << endl;
        }
    }
    cout << endl;

    /*
    for(int i = 0; i < 6; i++)
    {
        cout << gyakorisagok[i] << " ";
    }
    */

    //Gyakorisagok, foreach ciklussal
    for(int gy : gyakorisagok)
    {
        cout << gy << " ";
    }
    cout << endl << endl;

    //Relativ gyakorisagok
    for(int gy : gyakorisagok)
    {
        cout << (double)gy/N << " ";
    }
    cout << endl;

    return 0;
}
