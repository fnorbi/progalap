#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int n;

    cout << "n: ";
    cin >> n;

    for(int i = 1; i <= n; i++)
    {
        for(int j = 1; j <= n; j++)
        {
            cout << i * j << " ";
        }
        cout << endl;
    }

    //cout << sqrt(5);

    return 0;
}
