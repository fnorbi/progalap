#include <iostream>

using namespace std;

int main()
{
    int n;

    cout << "n: ";
    cin >> n;

    int i = 0;
    while(i * i <= n)
    {
        cout << i * i << " ";
        i++;
    }

    return 0;
}
