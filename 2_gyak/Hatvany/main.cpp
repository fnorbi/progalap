#include <iostream>

using namespace std;

int main()
{
    double a;
    int n;

    cout << "Alap: ";
    cin >> a;
    cout << "Kitevo: ";
    cin >> n;

    double hatvany = 1;

    for(int i = 0; i < n; i++)
    {
        hatvany *= a;
    }

    cout << a << "^" << n << ": " << hatvany << endl;

    return 0;
}
