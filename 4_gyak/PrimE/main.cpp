#include <iostream>

using namespace std;

int main()
{
    int p;
    bool primE;

    //cin >> p;

    for(p = 2; p <= 10000; p++)
    {
        int i = 2;
        while(i < p-1 && p % i != 0)
        {
            i++;
        }
        primE = i >= p-1;

        cout << p << " " << (primE ? "Prim!" : "Nem prim!") << endl;
        /*
        if(primE)
        {
            cout << p << " ";
        }
        */
    }
    cout << endl;

    /*
    if(primE)
    {
        cout << "Prim!" << endl;
    }
    else
    {
        cout << "Nem prim!" << endl;
    }
    */

    return 0;
}
