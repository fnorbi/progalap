#include <iostream>
#include <vector>

using namespace std;

struct Ember
{
    string nev;
    int mag;
};

int main()
{
    //Deklaracio
    int n;
    vector<Ember> emberek;
    bool azonosE;

    //Beolvasas
    cin >> n;
    emberek.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> emberek[i].nev >> emberek[i].mag;
    }

    //Feldolgozas = eldontes tetel
    int i = 0;
    while(i < n-1 && emberek[i].mag <= emberek[i+1].mag)
    {
        i++;
    }
    azonosE = i >= n-1;

    //Kiiratas
    if(azonosE)
    {
        cout << "Azonos!" << endl;
    }
    else
    {
        cout << "Nem azonos!" << endl;
    }

    return 0;
}
