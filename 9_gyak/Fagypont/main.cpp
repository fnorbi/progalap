#include <iostream>
#include <vector>

using namespace std;

void Beolvas(int &n, vector<int> &h)
{
    cin >> n;
    h.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> h[i];
    }
}

void Szetvalogatas(int n, vector<int> h,
                   int &dbNeg, int &dbPoz, int &dbNul,
                   vector<int> &negativak, vector<int> &pozitivak,
                   vector<int> &nullak)
{
   dbNeg = 0; dbPoz = 0; dbNul = 0;
   for(int i = 0; i < n; i++)
   {
       if(h[i] < 0)
       {
           dbNeg++;
           negativak.push_back(i);
       }
       else if(h[i] > 0)
       {
           dbPoz++;
           pozitivak.push_back(i);
       }
       else
       {
           dbNul++;
           nullak.push_back(i);
       }
   }
}

void Kiir(int dbNeg, int dbPoz, int dbNul,
          vector<int> negativak, vector<int> pozitivak,
          vector<int> nullak)
{
    for(int i = 0; i < dbNeg; i++)
    {
        cout << negativak[i] + 1 << " ";
    }
    cout << endl;
    for(int i = 0; i < dbPoz; i++)
    {
        cout << pozitivak[i] + 1 << " ";
    }
    cout << endl;
    for(int i = 0; i < dbNul; i++)
    {
        cout << nullak[i] + 1 << " ";
    }
}

int main()
{
    int n;
    vector<int> h;
    int dbNeg, dbPoz, dbNul;
    vector<int> negativak, pozitivak, nullak;

    Beolvas(n, h);

    Szetvalogatas(n, h, dbNeg, dbPoz, dbNul, negativak, pozitivak, nullak);

    Kiir(dbNeg, dbPoz, dbNul, negativak, pozitivak, nullak);

    return 0;
}
