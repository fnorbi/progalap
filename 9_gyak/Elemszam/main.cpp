#include <iostream>
#include <ctime>
#include <vector>
#include <set>

using namespace std;

const int CETLIK = 100;
const int HANYSZOR = 200;

bool volt(int szam, vector<int> v)
{
    int i = 0;
    while(i < v.size() && szam != v[i])
    {
        i++;
    }
    return i < v.size();
}

int main()
{
    srand(time(0));
    vector<int> v;
    set<int> s;

    cout << "Hanyszor: " << HANYSZOR << endl;
    for(int i = 0; i < HANYSZOR; i++)
    {
        int szam = rand() % CETLIK + 1;
        cout << szam << " ";
        if(!volt(szam, v))
        {
            v.push_back(szam);
        }
        s.insert(szam);
    }
    cout << endl << endl;

    cout << "Halmaz: " << s.size() << endl;
    set<int>:: iterator it;
    it = s.begin();
    while(it != s.end())
    {
        cout << *it << " ";
        it++;
    }
    cout << endl << endl;

    cout << "Halmaz: " << s.size() << endl;
    for(int e : s)
    {
        cout << e << " ";
    }
    cout << endl << endl;

    cout << "Vektor: " << v.size() << endl;
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }

    return 0;
}
