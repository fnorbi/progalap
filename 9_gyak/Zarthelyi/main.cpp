#include <iostream>
#include <vector>

using namespace std;

struct Tanulo
{
    string nev;
    int pont;
};

bool operator != (Tanulo t1, Tanulo t2)
{
    return t1.nev != t2.nev;
}

void Beolvas(int &n, int &m, vector<Tanulo> &zh, vector<Tanulo> &potzh)
{
    cin >> n;
    zh.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> zh[i].nev >> zh[i].pont;
    }
    cin >> m;
    potzh.resize(m);
    for(int i = 0; i < m; i++)
    {
        cin >> potzh[i].nev >> potzh[i].pont;
    }
}

bool Eleme(Tanulo e, int m, vector<Tanulo> x)
{
    int i = 0;
    while(i < m && e != x[i])
    {
        i++;
    }
    return i < m;
}

void Metszet(int n, int m, vector<Tanulo> zh, vector<Tanulo> potzh,
             int &db, vector<Tanulo> &mindketto)
{
    for(int i = 0; i < n; i++)
    {
        if(Eleme(zh[i], m, potzh))
        {
            db++;
            mindketto.push_back(zh[i]);
        }
    }
}

void Kiir(int db, vector<Tanulo> mindketto)
{
    cout << db << endl;
    for(int i = 0; i < db; i++)
    {
        cout << mindketto[i].nev << endl;
    }
}

int main()
{
    int n, m;
    vector<Tanulo> zh, potzh;
    int db;
    vector<Tanulo> mindketto;

    Beolvas(n, m, zh, potzh);

    Metszet(n, m, zh, potzh, db, mindketto);

    Kiir(db, mindketto);

    return 0;
}
