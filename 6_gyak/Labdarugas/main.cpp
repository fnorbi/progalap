#include <iostream>
#include <vector>

using namespace std;

struct Csapat
{
    int gyozelem, dontetlen;
};

int main()
{
    int n;
    vector<Csapat> csapatok;

    cin >> n;
    csapatok.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> csapatok[i].gyozelem >> csapatok[i].dontetlen;
    }

    //Maximum-kivalasztas
    int ind = 0;
    int pontszam = 2 * csapatok[0].gyozelem + csapatok[0].dontetlen;
    for(int i = 1; i < n; i++)
    {
        if(2 * csapatok[i].gyozelem + csapatok[i].dontetlen > pontszam)
        {
            ind = i;
            pontszam = 2 * csapatok[i].gyozelem + csapatok[i].dontetlen;
        }
    }

    cout << ind+1 << " " << pontszam << endl;

    return 0;
}
