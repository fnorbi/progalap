#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n;
    vector<int> a;
    int b;

    cin >> n;
    a.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> a[i];
    }
    cin >> b;

    bool van;
    int elso, masodik;

    int i, j;

    van = false;
    i = 0;
    while(i < n && !van)
    {
        j = i + 1;
        //Keresek az i. indexhez jo j. indexet
        while(j < n && !(b == a[i] * a[j]))
        {
            j++;
        }
        van = j < n;
        i++;
    }

    if(van)
    {
        elso = a[i-1];
        masodik = a[j];
        cout << elso << " " << masodik << endl;
    }
    else
    {
        cout << "Nincs ilyen!" << endl;
    }

    return 0;
}
