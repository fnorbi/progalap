#include <iostream>
#include <vector>

using namespace std;

struct Benzinkut
{
    int tav, menny;
};

int main()
{
    //Beszedes valtozonevek
    int tankolasokSzama;
    int kilometer;
    int benzin;
    int fogyasztas;
    vector<Benzinkut> tankolasok;

    cin >> kilometer >> tankolasokSzama >> benzin >> fogyasztas;
    tankolasok.resize(tankolasokSzama);
    for(int i = 0; i < tankolasokSzama; i++)
    {
        cin >> tankolasok[i].tav >> tankolasok[i].menny;
    }

    bool van;
    int ind;

    int elozo, most, i;
    elozo = benzin;
    most = elozo - tankolasok[0].tav/100*fogyasztas + tankolasok[0].menny;
    i = 1;
    while(i <= tankolasokSzama && !(most < elozo))
    {
        int tav;
        tav = tankolasok[i].tav - tankolasok[i-1].tav;
        elozo = most;
        most = most - tav/100*fogyasztas + tankolasok[i].menny;
        i++;
    }
    van = i <= tankolasokSzama;

    if(van)
    {
        ind = i - 1;
    }
    else
    {
        ind = -1;
    }

    cout << ind + 1 << endl;

    return 0;
}
